﻿using System.IO;
using Un4seen.Bass.AddOn.Tags;

namespace MediaPlayer
{
    public class EasyTags
    {
        public int BitRate;
        public int Freq;
        public string Artist;
        public string Album;
        public string Title;
        public double Length;

        public EasyTags(string file)
        {
            TAG_INFO tagInfo = new TAG_INFO();
            tagInfo = BassTags.BASS_TAG_GetFromFile(file);
            BitRate = tagInfo.bitrate;
            Freq = tagInfo.channelinfo.freq;
            Artist = tagInfo.artist;
            Album = tagInfo.album;
            Title = tagInfo.title;
            Length = tagInfo.duration;
            if (tagInfo.title == "")
                Title = Path.GetFileName(file);
        }
    }
}
